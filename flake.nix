{
  description = "Node dev environment";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let inherit (nixpkgs.lib) optional optionals;

        in pkgs.mkShell {
          buildInputs = with pkgs; [
            ruby_3_1
            rubyPackages_3_1.solargraph
            bundler
            nixfmt
            yaml-language-server
          ];
          shellHook = "";
        };
      });
}
